defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Hound.Helpers
  alias Auction.{Repo, Item}

  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)
    %{}
  end
  scenario_starting_state fn _state ->
    Hound.start_session
    Ecto.Adapters.SQL.Sandbox.checkout(Auction.Repo)
    Ecto.Adapters.SQL.Sandbox.mode(Auction.Repo, {:shared, self()})
    %{}
  end
  scenario_finalize fn _status, _state -> 
    Ecto.Adapters.SQL.Sandbox.checkin(Takso.Repo)
    Hound.end_session
  end

  given_ ~r/^the following items are on sale$/, fn state, %{table_data: table} ->
    IO.inspect table
    table
    |> Enum.map(fn item -> Item.changeset(%Item{}, item) end)
    |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
    {:ok, state}
  end

  and_ ~r/^I want to buy "(?<argument_one>[^"]+)"$/,
  fn state, %{argument_one: _argument_one} ->
    {:ok, state}
  end

  and_ ~r/^I open the Auction page$/, fn state ->
    navigate_to "/"
    {:ok, state}
  end

  and_ ~r/^I select the item$/, fn state ->
    {:ok, state}
  end

  and_ ~r/^I enter the bid of "(?<argument_one>[^"]+)"$/,
  fn state, %{argument_one: _argument_one} ->
    {:ok, state}
  end

  when_ ~r/^I submit the bid$/, fn state ->
    {:ok, state}
  end

  then_ ~r/^The price is updated for the item$/, fn state ->
    {:ok, state}
  end

  then_ ~r/^I should receive a rejection message$/, fn state ->
    {:ok, state}
  end
end
