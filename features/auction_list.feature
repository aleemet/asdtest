Feature: Auction list
    As a user
    I want to be able to see all the items on sale
    So that I can place a bid 

    Scenario: Placing a successful bid
        Given the following items are on sale
            | description | price | closingdate |
            | Roller Derby Brand Blade Skate (Size 7) | 29.00 | 29/01/2019 |
            | Chicago Bullet Speed Skate (Size 7) | 59.00 | 26/09/2019 |
            | Riedell Dart Derby Skates (Size 8) | 106.00 | 30/01/2019 |
        And I want to buy "Chicago Bullet Speed Skate (Size 7)"
        And I open the Auction page
        And I select the item
        And I enter the bid of "62.00"
        When I submit the bid
        Then The price is updated for the item

    Scenario: Placing an unsuccessful bid        
        Given the following items are on sale
            | description | price | closingdate |
            | Roller Derby Brand Blade Skate (Size 7) | 29.00 | 29/01/2019 |
            | Chicago Bullet Speed Skate (Size 7) | 59.00 | 26/09/2019 |
            | Riedell Dart Derby Skates (Size 8) | 106.00 | 30/01/2019 |
        And I want to buy "Riedell Dart Derby Skates (Size 8)"
        And I open the Auction page
        And I select the item
        And I enter the bid of "50.00"
        When I submit the bid
        Then I should receive a rejection message