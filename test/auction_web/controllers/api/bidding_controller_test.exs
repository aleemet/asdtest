defmodule AuctionWeb.Api.BiddingControllerTest do
  use AuctionWeb.ConnCase
  import Ecto.Query
  alias Ecto.Changeset
  alias Auction.Repo

  test "GET /api/items", %{conn: conn} do
    conn = get conn, "/api/items"
    assert [%{"description" => "", "closingdate" => "", "price" => 4.5}] = json_response(conn, 200)
  end
end