// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

// import socket from "./socket"

import Vue from "vue";
import axios from "axios";

new Vue({
  el: '#auction-app',
  data: {
    mode: 0,
    headers: {
        desc: "Description",
        price: "Price",
        date: "Closing Date",
        act: "Action"
    },
    items: [],
    biddable: null,
    newBid: 0
  },
  methods: {
      biddingView(item) {
        this.biddable = item;
        this.mode = 1;
      },
      itemsView() {
        this.biddable = null;
        this.newBid = 0;
        this.getItems()
        this.mode = 0;
      },
      bid () {
        if (this.newBid <= this.biddable.price) {
          alert("Bid too low!")
        }
        else {
          axios
            .post("/api/items/price/" + this.biddable.id, {
              price: parseFloat(this.newBid).toFixed(2)
            })
            .then(response => {
              console.log(response);
              alert("Bid successful!");
              this.itemsView();
            })
            .catch(error => console.log(error))
        }
      },
      getItems() {
        axios
          .get("/api/items")
          .then(response => {
            
            this.items = response.data.items;
          })
          .catch(error => console.log(error))
      }
  },
  created: function() {
    this.getItems()
  }
});