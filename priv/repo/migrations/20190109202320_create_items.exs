defmodule Auction.Repo.Migrations.CreateItems do
  use Ecto.Migration

  def change do
    create table(:items) do
      add :description, :string
      add :price, :float
      add :closingdate, :date

      timestamps()
    end

  end
end
