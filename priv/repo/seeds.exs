# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Auction.Repo.insert!(%Auction.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Auction.{Repo, Item}

[
    %Item{description: "Roller Derby Brand Blade Skate (Size 7)", price: 29.00, closingdate: Date.from_iso8601!("2019-01-29")},
    %Item{description: "Chicago Bullet Speed Skate (Size 7)", price: 59.00, closingdate: Date.from_iso8601!("2019-01-26")},
    %Item{description: "Riedell Dart Derby Skates (Size 8)", price: 106.00, closingdate: Date.from_iso8601!("2019-01-30")}
]
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)