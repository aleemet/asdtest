defmodule Auction.Item do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Poison.Encoder, only: [:id, :description, :price, :closingdate]}

  schema "items" do
    field :closingdate, :date
    field :description, :string
    field :price, :float

    timestamps()
  end

  @doc false
  def changeset(item, attrs) do
    item
    |> cast(attrs, [:description, :price, :closingdate])
    |> validate_required([:description, :price, :closingdate])
  end
end
