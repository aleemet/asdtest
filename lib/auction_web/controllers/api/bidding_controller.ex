defmodule AuctionWeb.Api.BiddingController do
    use AuctionWeb, :controller
    import Ecto.Query
    alias Ecto.Changeset
    alias Auction.{Repo, Item}

    def getItems(conn, _params) do
        q = from p in Item,
            select: p

        result = Repo.all q

        put_status(conn, 200) |> json(%{items: result})
    end

    def updatePrice(conn,  %{"id" => id, "price" => price}) do 
        item = Repo.get!(Item, id)
        item = Changeset.change item, %{price: String.to_float(price)}

        case Repo.update item do
            {:ok, _struct} -> put_status(conn, 200) |> json(%{msg: "Updated item price", price: price})
            {:error, _changeset} -> put_status(conn, 400) |> json(%{msg: "Failed to update item price", error: :error})
        end
    end

end