# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :auction,
  ecto_repos: [Auction.Repo]

# Configures the endpoint
config :auction, AuctionWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "zCxEarv6jaxWT2EKVfqEGmYY/E2jQMH1h7LgymUeYfLCV10UiBR303AkQJf9287+",
  render_errors: [view: AuctionWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Auction.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
